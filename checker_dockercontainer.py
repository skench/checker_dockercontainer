from typing import Optional

import docker
import os

def is_container_running(container_name: str) -> Optional[bool]:
    RUNNING = "running"
    docker_client = docker.from_env()
    try:
        container = docker_client.containers.get(container_name)
    except docker.errors.NotFound as exc:
        print(f"Check container name!\n{exc.explanation}")
    else:
        container_state = container.attrs["State"]
        return container_state["Status"] == RUNNING

if __name__ == "__main__":
    input_list = ["gitea"]
    length_list = len(input_list)
    dir_list =  ["~/Documents/gitlab.com/checker_dockercontainer"]
    length_dir_list = len(dir_list)
    x = 0
    y = 0
    while y < length_list:
        path = (dir_list[y])
        y += 1
        os.chdir(path)
    while x < length_list:
        container_name = (input_list[x])
        x += 1
    result = is_container_running(container_name)
    if result  == 1:
        print("Container up")
    else:
        print("Container down")
        with open('dockerfiles.txt') as my_list:
            for url in my_list:
                os.system('wget ' + url)
                os.system('docker-compose up -d')
